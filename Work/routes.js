exports.index = function(req, res) {
	var _         = require("underscore");

	res.render("index", { 
		_ : _,
		// Template data
		title: "Express"
	});
};


//get main categories
//also requestMapping("/categories")
exports.categories = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("categories", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Welcome to E-Shop",
				items : items
			});

			db.close();
		});
	});
};

//get subCategory which is selected by user
///requestMapping(/categories/:categoryId)
exports.subCategory = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var subCategoryStr = req.params.categoryId;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');
		
		//getting subCategory matches with child object of categories array
		collection.aggregate([
			{$match:{'categories.id':subCategoryStr}}, 
				{$project:{
					categories:{
						$filter: {input: '$categories', 
								  as: 'category', 
								  cond: {
									  $eq: ['$$category.id', subCategoryStr]
									}
								}},
								_id:0
							}
				}])
			.toArray(function(err, rtrn) {
				res.render("subCategory", { 
					_   :   _,
					// Template data
					title: rtrn[0].categories[0].name ,
					items: rtrn

				});

				db.close();
			});
	});
};


//get productlist by selected subcategory
///requestMapping(/categories/:categoryId/:name.:id)
exports.productlist = function(req, res) {
	debugger;
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var subCategoryStr = req.params.id;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		var products;

		//find products images
		collection.aggregate(
			[
				{$match:{"image_groups.view_type":"large", "primary_category_id":subCategoryStr}}, 
				{$project:
					{image_groups:
						{$filter: 
							{input: '$image_groups', as: 'image', cond: {
																			$eq: ['$$image.view_type', 'large']
																		}}
						},
						_id:0
					}
				}
			]).toArray(function(err, rtrn) {
				products = rtrn;
				db.close();
			});
		
		//find products by subCategory
		collection.find({"primary_category_id":subCategoryStr})
			.limit(20)
			.toArray(function(err, rtrn) {
				rtrn.name = req.params.name;
				res.render("productlist", { 
					_   :   _,
					// Template data
					title: req.params.name ,
					items: rtrn,
					products: products
				});

				db.close();
			});
	});
};


//get selected product
//requestMapping(/categories/:categoryId/:productName.:id/:name)
exports.product = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var productName = req.params.name;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products')
		
		collection.aggregate([{$match:{"name":productName}}]).toArray(function(err, rtrn) {

				rtrn[0].currencies = currencyArray();

				console.log('Currencies: '+rtrn[0].currencies );

				res.render("product", { 
					_   :   _,
					// Template data
					title: "Product Detail" ,
					items: rtrn
				});

				db.close();
			});
	});
	
};

//get price of product by different currencies
//requestMapping(/categories/:categoryId/:productName.:id/:name/:currency)
exports.currency = function(req, res) {
	var currencyCross = {"USDTOTRY": 3.69639306, "USDTOEUR": 0.939717145, "EURTOTRY": 3.93351667};
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var productName = req.params.name;
	var productPrice = 0;
	var currentCur;
	var currency = req.params.currency;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products')
		
		collection.aggregate([{$match:{"name":productName}}]).toArray(function(err, rtrn) {
				console.log("Fiyat: "+rtrn[0].price);
				productPrice = rtrn[0].price;
				currentCur = rtrn[0].currency;
				db.close();
				rtrn[0].price = getCalculatedPrice();
				rtrn[0].currency = currency;
				rtrn[0].currencies = currencyArray();
				res.render("product", { 
					_   :   _,
					title: "Product Detail" ,
					items: rtrn
				});
			});
	});


	function getCalculatedPrice(){
		var newStr = "Price: ";
		return currencyCalc(currentCur, currency, productPrice);
				
	}

	function currencyCalc(fromCur, toCur, productPrice){
		var lastVal = productPrice
		switch(fromCur){
			case "EUR":
			lastVal = toCur === "USD" ? productPrice / currencyCross["USDTOEUR"] : toCur === "TRY" ? productPrice * currencyCross["EURTOTRY"] : productPrice;
			break;

			case "TRY":
			lastVal = toCur === "USD" ? productPrice / currencyCross["USDTOTRY"] : toCur === "EUR" ? productPrice / currencyCross["EURTOTRY"] : productPrice;
			break;

			case "USD":
			lastVal = toCur === "EUR" ? productPrice * currencyCross["USDTOEUR"] : toCur === "TRY" ? productPrice * currencyCross["USDTOTRY"] : productPrice;
			break;
		}
		return lastVal = toFixed(lastVal, 2);
	}

	function toFixed(value, precision) {
		var power = Math.pow(10, precision || 0);
		return String(Math.round(value * power) / power);
	}
	
};


function currencyArray(){
	var curArr = ["USD", "EUR", "TRY"];
	return curArr;
}
	