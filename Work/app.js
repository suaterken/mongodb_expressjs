// Module dependencies.
var express = require("express")
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes");
var app     = express();

var breadcrumb = require('express-url-breadcrumb');

// use for every request  
app.use(breadcrumb());

// All environments
app.set("port", 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());

// App routes
app.get("/"     , breadcrumb(), routes.index);
app.get("/categories", breadcrumb(), routes.categories);
app.get("/categories/:categoryId", breadcrumb(), routes.subCategory);
app.get("/categories/:categoryId/:name.:id", breadcrumb(), routes.productlist);
app.get("/categories/:categoryId/:productName.:id/:name", breadcrumb(), routes.product);
app.get("/categories/:categoryId/:productName.:id/:name/:currency", routes.currency);

// Run server
try{
  
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
});
}catch(ex){
  console.log(ex.toString());
}
